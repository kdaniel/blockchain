import org.apache.commons.codec.digest.DigestUtils;

import java.util.List;

public class Block {

    private String hash;
    private String timeStamp;
    private String transactions;
    private Integer nonce;
    private String previousHash;

    public Block(String transactions, String previousHash, Integer difficulty) {
        this.previousHash = previousHash;
        this.timeStamp = String.valueOf(System.currentTimeMillis());
        this.transactions = transactions;
        this.nonce = 0;
        this.hash = miningBlock(difficulty);
    }

    private String miningBlock(Integer diff) {
        String target = new String(new char[diff]).replace('\0', '0');
        if(this.hash == null) {
            this.hash = "elso";
        }
        while(!hash.substring( 0, diff).equals(target)) {
            nonce++;
            hash = calculateHash();
        }
        return hash;
    }

    private String calculateHash() {
        return DigestUtils.sha256Hex(timeStamp + transactions + nonce + previousHash);
    }

    /*public Block(String timeStamp) {
        this.hash = hash;
        this.timeStamp = timeStamp;
    }*/

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    @Override
    public String toString() {
        return "Block{" +
                "hash='" + hash + '\'' +
                ", timeStamp='" + timeStamp + '\'' +
                ", transactions='" + transactions + '\'' +
                ", nonce=" + nonce +
                ", previousHash='" + previousHash + '\'' +
                '}';
    }
}
