import java.util.ArrayList;
import java.util.List;

public class BlockChain {
    private List<Block> blocks;
    private Integer difficulty;
    private Integer miningReward;
    private String pendingTransactions;

    public BlockChain(Integer difficulty, Integer miningReward) {
        this.blocks = new ArrayList<>();
        this.difficulty = difficulty;
        this.miningReward = miningReward;
        this.pendingTransactions = "";
        mineGenesisBlock();
    }

    private void mineGenesisBlock() {
        blocks.add(new Block("genesis block", "0", 4));
    }

    public List<Block> getBlocks() {
        return blocks;
    }

    public void setBlocks(List<Block> blocks) {
        this.blocks = blocks;
    }
}
